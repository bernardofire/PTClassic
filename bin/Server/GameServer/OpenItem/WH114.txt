//		아이템 정보		//


*이름		"워 마울"
*Name		"War Maul"
*코드		"WH114"

///////////////	공통사항		 ////////////

*내구력		80 105
*무게		47
*가격		66000

///////////////	원소		/////////////

*생체		
*불		
*냉기		
*번개		
*독		

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		22 24	40 46
*사정거리	
*공격속도	5
*명중력		105 120
*크리티컬	16

//////////////	방어성능		/////////////
// 추가요인

*흡수력		
*방어력		
*블럭율		7 7

//////////////	신발성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생	
*근력재생	
*생명력추가	
*기력추가	
*근력추가	
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		64
*힘		138
*정신력		
*재능		80
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승	
*기력상승	
*근력상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능
//**특화 Pilgrim //

**특화랜덤 Knight Atalanta Fighter Pikeman

**공격력	0	6
**명중력	1	3
**크리티컬	6
**공격속도	2

*연결파일	"name\WH114.zhoon"